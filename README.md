ESS Reference & Lookup Tables
--

# Directory description

* init: will be loaded during the initialization. The facility constant configuration.
* supercycles: will be loaded upon the selection (memorized PV with the supercycle table selection). The dynamic routines/cycles (runtime).
* tools: productivity enhancements.
* doc: other documents and references.

# Supercycle manual
* https://confluence.esss.lu.se/display/HAR/Supecycle

# References
* Description of Modes for ESS Accelerator Operation: ESS-0038258
  * $(ESS-0038258->Revision) == $ProtVer
* Beam Configuration: https://confluence.esss.lu.se/display/ABC/Beam+Configuration
* Master events according to: ESS-1837307: Time Structure of the Proton Beam Pulses in the LINAC.

