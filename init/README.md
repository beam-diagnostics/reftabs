Init
--

# databuffer-ess.json
* The databuffer enumeration

# accelerator-operation-ess.json
* The accelerator operation enumeration (not included into the databuffer)

# mevts-ess.json
* Master events according to: ESS-1837307: Time Structure of the Proton Beam Pulses in the LINAC.
